import { HttpErrorResponse } from '@angular/common/http';
import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Reuniao } from '@app/models/reuniao';
import { ReuniaoService } from '@app/services/reuniao.service';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { FormularioReuniaoComponent } from '../formulario-reuniao/formulario-reuniao.component';

@Component({
  selector: 'lista-reunioes',
  templateUrl: './lista-reunioes.component.html',
  styleUrls: ['./lista-reunioes.component.css']
})
export class ListaReunioesComponent implements AfterViewInit {

  displayedColumns: string[] = ['titulo', 'assunto', 'responsavel', 'data', 'hora', 'acoes']
  reunioes: Reuniao[] = []  
  length: number = 0
  filtroTitulo: string = ''
  carregando: boolean = true
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  formularioFiltro: FormGroup = this.fb.group({
    titulo: ['']    
  })

  constructor(
    private reunioesService: ReuniaoService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private fb: FormBuilder
  ) { }  
  
  ngAfterViewInit(): void {
    this.paginator.pageSize = 5    
    this.buscarTodos(0,'data')
  }

  buscarTodos(pageNumber: number, sortField: string, filters?: string){
    this.carregando = true
    this.reunioesService.buscarTodos(pageNumber, this.paginator.pageSize, sortField, filters)
      .subscribe({
        next: (dados: any) => {          
          this.reunioes = dados.reunioes
          this.length = dados.page.size
          this.carregando = false
        },
        error: (err: HttpErrorResponse) => {
          if(err.status == 400 )
            this.snackBar.open('Nenhuma reunião encontrada!', 'ok', {duration: 5000})
          else
            this.snackBar.open(err.message, 'ok', {duration: 5000})          
          this.carregando = false
        },
      }
    )    
  }

  paginacao(event: PageEvent){    
    this.buscarTodos(event.pageIndex, 'data')
  }

  editar(reuniao: Reuniao){    
    const dialogRef = this.dialog.open(FormularioReuniaoComponent, { data: reuniao })
    dialogRef.afterClosed().subscribe(() => this.buscarTodos(0, 'data'))
  }
  
  cadastrar(){
    const dialogRef = this.dialog.open(FormularioReuniaoComponent)
    dialogRef.afterClosed().subscribe(() => this.buscarTodos(0, 'data'))
  }

  excluir(reuniao: Reuniao){
    const confirmDialogRef = this.dialog.open(ConfirmDialogComponent, { data: 'Deseja realmente excluir esta reunião?' })
    confirmDialogRef.afterClosed().subscribe((confirmou) => {      
      if(confirmou)
        this.reunioesService.excluir(reuniao.id)
          .subscribe(
            () => {
              this.snackBar.open(`Reunião excluída`, 'ok', {duration: 5000})
              this.buscarTodos(0, 'data')
            },
            (err: Error) => this.snackBar.open(err.message, 'ok', {duration: 5000}))  
    })
  }

  filtrar(){
    let titulo = this.formularioFiltro.get('titulo')?.value
    if(titulo){
      let filtro = `titulo=${titulo}`    
      this.buscarTodos(0, 'data', filtro)
    }else{
      this.buscarTodos(0, 'data')
    }
  }

}
