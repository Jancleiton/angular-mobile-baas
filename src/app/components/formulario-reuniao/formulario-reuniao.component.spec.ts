import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioReuniaoComponent } from './formulario-reuniao.component';

describe('FormularioReuniaoComponent', () => {
  let component: FormularioReuniaoComponent;
  let fixture: ComponentFixture<FormularioReuniaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormularioReuniaoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioReuniaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
