import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Reuniao } from '@app/models/reuniao';
import { ReuniaoService } from '@app/services/reuniao.service';

@Component({
  selector: 'formulario-reuniao',
  templateUrl: './formulario-reuniao.component.html',
  styleUrls: ['./formulario-reuniao.component.css']
})
export class FormularioReuniaoComponent implements OnInit {      
  
  formularioReuniao: FormGroup = this.fb.group({
    id: [null],
    titulo: ['', Validators.required],
    assunto: ['', Validators.required],
    responsavel: ['', Validators.required],
    data: ['', Validators.required],    
    horario: ['', Validators.required],    
  }) 
  
  constructor(
    @Inject(MAT_DIALOG_DATA) private reuniao: Reuniao,
    private reuniaoService: ReuniaoService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<FormularioReuniaoComponent>,
    private snackBar: MatSnackBar
    ) {
     }

  ngOnInit(): void {
    if(this.reuniao) 
      this.formularioReuniao.setValue(this.reuniao) 
  }

  fechar(){
    this.dialogRef.close(false)
  }

  salvar(){    
    let reuniao: Reuniao = this.formularioReuniao.value    
    if(reuniao.id)
      this.atualizar(reuniao)
    else
      this.criar(reuniao)
  }

  criar(reuniao: Reuniao){    
    this.reuniaoService.salvar(reuniao)
      .subscribe(
        () => {
          this.snackBar.open(`Reunião salva`, 'ok', {duration: 5000})
          this.dialogRef.close()
        },
        err => this.snackBar.open(err, 'ok', {duration: 5000}))
    }
    
  atualizar(reuniao: Reuniao){
    this.reuniaoService.atualizar(reuniao)
      .subscribe(
        () => {
          this.snackBar.open(`Reunião atualizada`, 'ok', {duration: 5000})
          this.dialogRef.close()
        },
        err => this.snackBar.open(err, 'ok', {duration: 5000}))
  }

}
