export class Reuniao {
    id: string = ''
    titulo: string = ''
    assunto: string = ''
    responsavel: string = ''
    data: string = ''
    horario: string = ''
}
