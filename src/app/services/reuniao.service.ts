import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { KEYS } from '@app/keys';
import { Reuniao } from '@app/models/reuniao';

@Injectable({
  providedIn: 'root'
})
export class ReuniaoService {

  MOBILE_BAAS_URL = KEYS.MOBILE_BAAS_URL   
  TABELA = KEYS.TABLE_NAME

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'MOBILEBAASKEY': KEYS.MOBILE_BAAS_KEY
    })
  }

  constructor(private http: HttpClient) { }

  salvar(reuniao: Reuniao){
    return this.http.post(`${this.MOBILE_BAAS_URL}?table=${this.TABELA}`, reuniao, this.httpOptions)
  }
  
  atualizar(reuniao: Reuniao){
    return this.http.put(`${this.MOBILE_BAAS_URL}?table=${this.TABELA}`, reuniao, this.httpOptions)
  }
  
  excluir(id: string){
    return this.http.delete(`${this.MOBILE_BAAS_URL}${id}?table=${this.TABELA}`, this.httpOptions)
  }
  
  buscarPorId(id: string){
    return this.http.get(`${this.MOBILE_BAAS_URL}${id}?table=${this.TABELA}`, this.httpOptions)
  }
  
  buscarTodos(pageNumber: number, totalRecordsPerPage: number, sortField?: string, filters?: string){
    let parametros: string = ''
    if(sortField) parametros += `&sortField=${sortField}`
    if(filters) parametros += `&filters=${filters}`
    return this.http.get(`${this.MOBILE_BAAS_URL}find?table=${this.TABELA}&pageNumber=${pageNumber}&totalRecordsPerPage=${totalRecordsPerPage}${parametros}`, this.httpOptions)
  }


}
